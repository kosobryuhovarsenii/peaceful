    <section class="consultation">
      <div class="container">
        <div class="consultation-block">
          <div class="consultation__text">
            <span class="consultation__title">Получите бесплатную консультацию!</span>
            <span class="consultation__subtitle">Отправте заявку и наши сотрудники свяжутся с Вами прямо сейчас</span>
          </div>
          <div class="consultation-buttons">
            <a href="#" class="consultation-buttons__call dialog-button wow bounceInRight" data-wow-delay="0.2s">заказать звонок</a>
            <a href="#" class="consultation-buttons__entry dialog-button wow bounceInRight" data-wow-delay="0.4s">записаться на прием</a>
          </div>
        </div>
      </div>
    </section>