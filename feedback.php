<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css">
  <title>Миролюбец</title>
</head>

<body>

  <?php include 'header.php'; ?>

  <?php include 'navbar.php'; ?>

  <main>

    <section class="feedback">
      <div class="container">
        <div class="feedback-block">
          <span class="feedback__title">Отзывы наших клиентов</span>
          <div class="feedback-block__cards">
            <div class="feedback-card wow fadeInUp" data-wow-delay="0.1s">
              <div class="feedback-card__review">
                <img src="img/feedback/rating.png" alt="рейтинг" class="feedback-card__review-rating">
                <span class="feedback-card__review-text">С другой стороны консультация с широким активом в значительной
                  степени обуславливает создание направлений прогрессивного развития. Значимость этих проблем настолько
                  очевидна, что консультация с широким активом представляет собой интересный эксперимент проверки
                  существенных финансовых и административных условий. Идейные соображения высшего порядка, а также
                  сложившаяся структура организации способствует подготовки и реализации соответствующий условий
                  активизации. Таким образом дальнейшее развитие различных форм деятельности позволяет оценить значение
                  дальнейших направлений развития. Таким образом рамки и место обучения кадров обеспечивает широкому
                  кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных
                  задач. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей
                  активности в значительной степени обуславливает создание модели развития.</span>
                <img src="img/feedback/square.png" alt="квадрат" class="feedback-card__review-square">
              </div>
              <div class="feedback-card__human">
                <img src="img/feedback/woman.png" alt="фото" class="feedback-card__human-photo">
                <div class="feedback-card__human-text">
                  <span class="feedback-card__human-username">Ольга Петрова</span>
                  <span class="feedback-card__human-profession">Web-дизайнер</span>
                </div>
              </div>
            </div>
            <!-- /.feedback-card -->
            <div class="feedback-card wow fadeInUp" data-wow-delay="0.2s">
              <div class="feedback-card__review">
                <img src="img/feedback/rating.png" alt="рейтинг" class="feedback-card__review-rating">
                <span class="feedback-card__review-text">С другой стороны консультация с широким активом в значительной
                  степени обуславливает создание направлений прогрессивного развития. Значимость этих проблем настолько
                  очевидна, что консультация с широким активом представляет собой интересный эксперимент проверки
                  существенных финансовых и административных условий. Идейные соображения высшего порядка, а также
                  сложившаяся структура организации способствует подготовки и реализации соответствующий условий
                  активизации. Таким образом дальнейшее развитие различных форм деятельности позволяет оценить значение
                  дальнейших направлений развития. Таким образом рамки и место обучения кадров обеспечивает широкому
                  кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных
                  задач. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей
                  активности в значительной степени обуславливает создание модели развития.</span>
                <img src="img/feedback/square.png" alt="квадрат" class="feedback-card__review-square">
              </div>
              <div class="feedback-card__human">
                <img src="img/feedback/woman.png" alt="фото" class="feedback-card__human-photo">
                <div class="feedback-card__human-text">
                  <span class="feedback-card__human-username">Ольга Петрова</span>
                  <span class="feedback-card__human-profession">Web-дизайнер</span>
                </div>
              </div>
            </div>
            <!-- /.feedback-card -->
            <div class="feedback-card wow fadeInUp" data-wow-delay="0.3s">
              <div class="feedback-card__review">
                <img src="img/feedback/rating.png" alt="рейтинг" class="feedback-card__review-rating">
                <span class="feedback-card__review-text">С другой стороны консультация с широким активом в значительной
                  степени обуславливает создание направлений прогрессивного развития. Значимость этих проблем настолько
                  очевидна, что консультация с широким активом представляет собой интересный эксперимент проверки
                  существенных финансовых и административных условий. Идейные соображения высшего порядка, а также
                  сложившаяся структура организации способствует подготовки и реализации соответствующий условий
                  активизации. Таким образом дальнейшее развитие различных форм деятельности позволяет оценить значение
                  дальнейших направлений развития. Таким образом рамки и место обучения кадров обеспечивает широкому
                  кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных
                  задач. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей
                  активности в значительной степени обуславливает создание модели развития.</span>
                <img src="img/feedback/square.png" alt="квадрат" class="feedback-card__review-square">
              </div>
              <div class="feedback-card__human">
                <img src="img/feedback/woman.png" alt="фото" class="feedback-card__human-photo">
                <div class="feedback-card__human-text">
                  <span class="feedback-card__human-username">Ольга Петрова</span>
                  <span class="feedback-card__human-profession">Web-дизайнер</span>
                </div>
              </div>
            </div>
            <!-- /.feedback-card -->
          </div>

        </div>
      </div>
    </section>

    <?php include 'consultation.php'; ?>

  </main>

  <?php include 'footer.php'; ?>
  
</body>

</html>