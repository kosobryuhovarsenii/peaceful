  <footer class="footer">
    <div class="container">
      <div class="footer-block">
        <div class="footer-description">
          <div class="footer-description__logo">
            <img src="img/footer/footer-logo.png" class="footer-description__image" alt="logo">
            <div class="footer-description__block-text">
              <span class="footer-description__title">Миролюбец</span>
              <span class="footer-description__subtitle">закроем ваши крылья</span>
            </div>
          </div>
          <span class="footer-description__text">С другой стороны консультация с широким активом в значительной
            степени обуславливает создание направлений прогрессивного развития. Значимость этих проблем настолько
            очевидна, что консультация с широким активом представляет собой интересный эксперимент проверки
            существенных финансовых и административных условий. Идейные соображения высшего порядка, а также
            сложившаяся структура организации способствует подготовки и реализации соответствующий условий
            активизации. Таким образом дальнейшее развитие различных форм деятельности позволяет оценить значение
            дальнейших направлений развития. Таким образом рамки и место обучения кадров обеспечивает широкому кругу
            (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных задач.
            Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей активности в
            значительной степени обуславливает создание модели развития.</span>
          <div class="footer-description__social">
            <a href="#" class="footer-description__social-item">
              <img src="img/footer/footer-vk.png" alt="vk">
            </a>
            <a href="#" class="footer-description__social-item">
              <img src="img/footer/footer-facebook.png" alt="facebook">
            </a>
            <a href="#" class="footer-description__social-item">
              <img src="img/footer/footer-instagram.png" alt="instagram">
            </a>
          </div>
        </div>
        <div class="footer-about">
          <span class="footer-about__title">О компании</span>
          <ul class="footer-about__list">
            <li>
              <a href="#" class="footer-about__list-item">Новости</a>
            </li>
            <li>
              <a href="#" class="footer-about__list-item">Анонсы</a>
            </li>
            <li>
              <a href="#" class="footer-about__list-item dialog-button">Контакты</a>
            </li>
          </ul>
        </div>
        <div class="footer-service">
          <span class="footer-service__title">Наши услуги</span>
          <div class="footer-service__block">
            <ul class="footer-service__list">
              <li>
                <a href="#" class="footer-service__list-item">Психология</a>
              </li>
              <li>
                <a href="#" class="footer-service__list-item">Психоаналитика</a>
              </li>
              <li>
                <a href="#" class="footer-service__list-item">Услуги медиатора</a>
              </li>
            </ul>
            <ul class="footer-service__list">
              <li>
                <a href="#" class="footer-service__list-item">Психология</a>
              </li>
              <li>
                <a href="#" class="footer-service__list-item">Психоаналитика</a>
              </li>
              <li>
                <a href="#" class="footer-service__list-item">Услуги медиатора</a>
              </li>
            </ul>
          </div>
        </div>
      </div>
      <div class="footer-signature">
        <span class="footer-signature__text">Создание и продвижение сайта:</span>
        <span class="footer-signature__name">Glo Academy</span>
      </div>
    </div>
  </footer>

  <div class="modal" id="modal">
    <div class="modal-success" id="success">
      <h2 class="modal-success__title"></h2>
    </div>
    <div class="modal-dialog" id="dialog">
      <div class="modal-column">
        <div class="modal-form">
          <button class="modal-dialog__close" id="close">&times;</button>
          <span class="modal-dialog__title">Заказать обратный звонок</span>
          <span class="modal-dialog__subtitle">Заполните поля ниже - мы свяжемся с Вами </span>
          <form action="#" method="post" class="form modal-dialog__form" id="modal-form" onsubmit="ym(55087507, 'reachGoal', 'form_submit'); return true;">
            <div class="modal__block-input block-input">
              <input type="text" class="input modal-dialog__input" id="user_name" name="user_name" placeholder="Ваше имя">
            </div>
            <div class="modal__block-input block-input">
              <input type="text" class="input modal-dialog__input phone" id="user_phone" name="user_phone" placeholder="Ваш телефон">
            </div>
            <div class="modal__block-input block-input">
              <input type="text" class="input modal-dialog__input" id="user_email" name="user_email" placeholder="Ваша почта">
            </div>
            <button type="submit" class="button modal-dialog__button">Отправить</button>
          </form>
        </div>
      </div>
    </div>
    <!-- /.modal-dialog -->
  </div>

  <script src="js/jquery-3.4.1.min.js"></script>
  <script src="js/burger-menu.js"></script>
  <script src="js/modal.js"></script>
  <script src="js/jquery.validate.min.js"></script>
  <script src="js/jquery.maskedinput.min.js"></script>
  <script src="js/scripts.js"></script>
  <script src="js/wow.min.js"></script>

  <script>
    // Инициализаия библиотеки WOW.js для анимации
    new WOW().init();
    // Маска для телефона
    $(document).ready(function () {
      $('.phone').mask('+7 (999) 999-99-99');
    });
  </script>