  <header class="header">
    <div class="container">
      <div class="header-block">
        <div class="header-block__information">
          <div class="header-phone">
            <img src="img/header/header-phone.png" alt="телефон" class="header-phonek__icon">
            <a href="tel:8(950)321-65-55" class="header-phone__text">8 (950) 321-65-55</a>
          </div>
          <div class="header-address">
            <img src="img/header/header-marker.png" alt="" class="header-address__icon">
            <span class="header-address__text">Ул. Мира 6, дом 1, офис 4.</span>
          </div>
        </div>
        <!-- /.header-block__information -->
        <div class="header-enter">
          <a href="#" class="header-enter__login">Логин</a>
          <a href="#" class="header-enter__registration">Регистрация</a>
        </div>
        <!-- /.header-enter -->
      </div>
    </div>
  </header>