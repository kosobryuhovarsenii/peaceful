<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css">
  <title>Миролюбец</title>
</head>

<body>

  <!-- Yandex.Metrika counter --> <script type="text/javascript" > (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)}; m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)}) (window, document, "script", "https://cdn.jsdelivr.net/npm/yandex-metrica-watch/tag.js", "ym"); ym(55087507, "init", { clickmap:true, trackLinks:true, accurateTrackBounce:true, webvisor:true, trackHash:true }); </script> <noscript><div><img src="https://mc.yandex.ru/watch/55087507" style="position:absolute; left:-9999px;" alt="" /></div></noscript> <!-- /Yandex.Metrika counter -->

  <?php include 'header.php'; ?>

  <?php include 'navbar.php'; ?>

  <main>

    <section class="hero">
      <div class="container">
        <div class="hero-block">
          <div class="hero-text wow bounceInLeft" data-wow-delay="0.2s">
            <h2 class="hero-text__subtitle">Центр психологической поддержки</h2>
            <h1 class="hero-text__title">Миролюбец</h1>
            <span class="hero-text__description">Центр работает в Абакане. Работаем по скайпу по миру, он-лайн
              консультации. По медиации-Сибирь.</span>
            <a href="#" class="hero-text__button dialog-button">связаться с нами</a>
          </div>
        </div>
      </div>
    </section>

    <section class="about" id="about">
      <div class="container">
        <div class="about-block">
          <span class="about__title">Почему мы?</span>
          <div class="about-cards">
            <div class="about-cards__card wow fadeInUp" data-wow-delay="0.1s">
              <div class="about-cards__block">
                <div class="about-cards__image">
                  <img src="img/about/hero-lamp.png" alt="лампа">
                </div>
              </div>
              <div class="about-cards__text">
                <span class="about-cards__title">Инновационный <br> подход</span>
                <span class="about-cards__description">С другой стороны консультация с широким активом в значительной
                  степени обуславливает создание направлений прогрессивного развития. Значимость этих проблем настолько
                  очевидна, что консультация с широким активом представляет собой интересный эксперимент проверки
                  существенных финансовых и административных условий. Идейные соображения высшего порядка, а также
                  сложившаяся структура организации способствует подготовки и реализации соответствующий условий
                  активизации. Таким образом дальнейшее развитие различных форм деятельности позволяет оценить значение
                  дальнейших направлений развития. Таким образом рамки и место обучения кадров обеспечивает широкому
                  кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных
                  задач. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей
                  активности в значительной степени обуславливает создание модели развития.</span>
                <a href="#" class="about-cards__button dialog-button">подробней</a>
              </div>
            </div>
            <!-- /.about-cards__card -->
            <div class="about-cards__card wow fadeInUp" data-wow-delay="0.2s">
              <div class="about-cards__block">
                <div class="about-cards__image">
                  <img src="img/about/about-reliability.png" alt="надежность">
                </div>
              </div>
              <div class="about-cards__text">
                <span class="about-cards__title">Более 10 лет <br> опыта работы</span>
                <span class="about-cards__description">С другой стороны консультация с широким активом в значительной
                  степени обуславливает создание направлений прогрессивного развития. Значимость этих проблем настолько
                  очевидна, что консультация с широким активом представляет собой интересный эксперимент проверки
                  существенных финансовых и административных условий. Идейные соображения высшего порядка, а также
                  сложившаяся структура организации способствует подготовки и реализации соответствующий условий
                  активизации. Таким образом дальнейшее развитие различных форм деятельности позволяет оценить значение
                  дальнейших направлений развития. Таким образом рамки и место обучения кадров обеспечивает широкому
                  кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных
                  задач. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей
                  активности в значительной степени обуславливает создание модели развития.</span>
                <a href="#" class="about-cards__button dialog-button">подробней</a>
              </div>
            </div>
            <!-- /.about-cards__card -->
            <div class="about-cards__card wow fadeInUp" data-wow-delay="0.3s">
              <div class="about-cards__block">
                <div class="about-cards__image">
                  <img src="img/about/about-all.png" alt="разнообразие">
                </div>
              </div>
              <div class="about-cards__text">
                <span class="about-cards__title">Разнообразие <br> услуг в одном месте</span>
                <span class="about-cards__description">С другой стороны консультация с широким активом в значительной
                  степени обуславливает создание направлений прогрессивного развития. Значимость этих проблем настолько
                  очевидна, что консультация с широким активом представляет собой интересный эксперимент проверки
                  существенных финансовых и административных условий. Идейные соображения высшего порядка, а также
                  сложившаяся структура организации способствует подготовки и реализации соответствующий условий
                  активизации. Таким образом дальнейшее развитие различных форм деятельности позволяет оценить значение
                  дальнейших направлений развития. Таким образом рамки и место обучения кадров обеспечивает широкому
                  кругу (специалистов) участие в формировании позиций, занимаемых участниками в отношении поставленных
                  задач. Идейные соображения высшего порядка, а также постоянный количественный рост и сфера нашей
                  активности в значительной степени обуславливает создание модели развития.</span>
                <a href="#" class="about-cards__button dialog-button">подробней</a>
              </div>
            </div>
            <!-- /.about-cards__card -->
          </div>
        </div>
      </div>
    </section>

    <?php include 'service.php'; ?>

    <section class="achievement">
      <div class="container">
        <div class="achievement-block">
          <div class="achievement-block__item wow zoomIn" data-wow-delay="0.1s">
            <div class="achievement-block__image">
              <img src="img/achievement/human.png" alt="клиент">
            </div>
            <span class="achievement-block__title">+900</span>
            <span class="achievement-block__subtitle">Клиентов</span>
          </div>
          <!-- /.achievement-block__item -->
          <div class="achievement-block__item wow zoomIn" data-wow-delay="0.2s">
            <div class="achievement-block__image">
              <img src="img/achievement/lists.png" alt="программа">
            </div>
            <span class="achievement-block__title">30</span>
            <span class="achievement-block__subtitle">Программ</span>
          </div>
          <!-- /.achievement-block__item -->
          <div class="achievement-block__item wow zoomIn" data-wow-delay="0.3s">
            <div class="achievement-block__image">
              <img src="img/achievement/achievement.png" alt="сертификат">
            </div>
            <span class="achievement-block__title">30</span>
            <span class="achievement-block__subtitle">Сертификатов</span>
          </div>
          <!-- /.achievement-block__item -->
          <div class="achievement-block__item wow zoomIn" data-wow-delay="0.4s">
            <div class="achievement-block__image">
              <img src="img/achievement/expert.png" alt="специалист">
            </div>
            <span class="achievement-block__title">9</span>
            <span class="achievement-block__subtitle">Специалистов</span>
          </div>
          <!-- /.achievement-block__item -->
        </div>
      </div>
    </section>

    <?php include 'consultation.php'; ?>

  </main>

  <?php include 'footer.php'; ?>

</body>

</html>