$(document).ready(function(){
  var button = $(".dialog-button");
  var modal = $("#modal");
  var close = $("#close");

  button.on("click", function(event){
    event.preventDefault();
    $("#dialog").addClass("modal-dialog_active");
    modal.addClass("modal_active");
  });

  close.on("click", function(){
    $("#dialog").removeClass("modal-dialog_active");
    modal.removeClass("modal_active");
  });
});