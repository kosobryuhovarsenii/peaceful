$(document).ready(function(){
// Валидация форм
  $("#modal-form").validate({
    rules: {
      user_name: {
        required: true,
        minlength: 2,
        maxlength: 16
      },
      user_phone: {
        required: true
      },
      user_email: {
        required: true,
        email: true
      },
    },
    messages: {
      user_name: "Заполните поле",
      user_phone: "Заполните поле",
      user_email: "Заполните поле",
    },
    submitHandler: function(){
      $.ajax({
          type: "POST",
          url: "mailer/mail.php",
          data: $('form').serialize(),
          success: function(data){
            console.log(data);
            $(".modal-success__title").html("Спасибо за заявку, скоро мы вам перезвоним!");
            $("#dialog").removeClass("modal-dialog_active");
            $("#success").addClass("modal-success_active");
            setTimeout(function(){
              $("#success").removeClass("modal-success_active")
              $("#modal").removeClass("modal_active")
            }, 3000);
            $("#user_name").val('');
            $("#user_phone").val('');
            $("#user_email").val('');
          },
          error: function(jqXHR, textStatus){
            console.log(jqXHR + ": " + textStatus);
          }
      });
    }
  });
});