  <nav class="navbar">
    <div class="container">
      <div class="navbar-block">
        <div class="navbar-logo">
          <div class="navbar-logo__image">
            <img src="img/navbar/navbar-logo.png" alt="Логотип">
          </div>
          <div class="navbar-logo__text">
            <span class="navbar-logo__title">Миролюбец</span>
            <span class="navbar-logo__subtitle">закроем ваши крылья</span>
          </div>
        </div>
        <!-- /.navbar__logo -->
        <div class="navbar-menu">
          <div class="navbar-menu__links">
            <a href="index.php" class="navbar-menu__links-item wow zoomIn" data-wow-delay="0.04s">главная</a>
            <a href="#about" class="navbar-menu__links-item wow zoomIn" data-wow-delay="0.08s">о компании</a>
            <a href="news.php" class="navbar-menu__links-item wow zoomIn" data-wow-delay="0.12s">новости</a>
            <a href="services.php" class="navbar-menu__links-item wow zoomIn" data-wow-delay="0.16s">услуги</a>
            <a href="feedback.php" class="navbar-menu__links-item wow zoomIn" data-wow-delay="0.2s">отзывы</a>
            <a href="#" class="navbar-menu__links-item wow dialog-button zoomIn" data-wow-delay="0.24s">контакты</a>
          </div>
          <a href="#" class="navbar-menu__btn">
            <span></span>
          </a>
        </div>

        <!-- /.navbar-menu -->
        <a href="#" class="navbar-callback dialog-button">
          <img src="img/navbar/phone.png" class="navbar-callback__image" alt="телефон">
        </a>
        <!-- /.navbar-callback -->
      </div>
    </div>
  </nav>