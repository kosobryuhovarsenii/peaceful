<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css">
  <title>Миролюбец</title>
</head>

<body>

  <?php include 'header.php'; ?>

  <?php include 'navbar.php'; ?>

  <main>

    <section class="news">
      <div class="container">
        <div class="news-block">
          <div class="news-newsfeed">
            <span class="news-newsfeed__title">Наши новости</span>
            <div class="news-newsfeed__block-card wow fadeInUp" data-wow-delay="0.1s">
              <div class="news-newsfeed__card">
                <div class="news-newsfeed__block-data">
                  <span class="news-newsfeed__data">
                    5
                    <span class="news-newsfeed__data-text">авг</span>
                  </span>
                </div>
                <div class="news-newsfeed__card-image">
                  <img src="img/news/news-photo.jpg" alt="фото">
                </div>
                <span class="news-newsfeed__card-title">Мы открыли новый офис</span>
                <span class="news-newsfeed__card-description">С другой стороны консультация с широким активом в
                  значительной степени обуславливает создание направлений прогрессивного развития. Значимость этих
                  проблем настолько очевидна, что консультация с широким активом представляет собой интересный
                  эксперимент проверки существенных финансовых и административных условий. Идейные соображения высшего
                  порядка, а также сложившаяся структура организации способствует подготовки и реализации
                  соответствующий условий активизации. Таким образом дальнейшее развитие различных форм деятельности
                  позволяет оценить значение дальнейших направлений развития. Таким образом рамки и место обучения
                  кадров обеспечивает широкому кругу (специалистов) участие в формировании позиций, занимаемых
                  участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также постоянный
                  количественный рост и сфера нашей активности в значительной степени обуславливает создание модели
                  развития.</span>
                <a href="#" class="news-newsfeed__card-button">подробней</a>
              </div>
              <!-- /.news-newsfeed__card -->
              <div class="news-newsfeed__card">
                <div class="news-newsfeed__block-data">
                  <span class="news-newsfeed__data">
                    5
                    <span class="news-newsfeed__data-text">авг</span>
                  </span>
                </div>
                <div class="news-newsfeed__card-image">
                  <img src="img/news/news-photo.jpg" alt="фото">
                </div>
                <span class="news-newsfeed__card-title">Мы открыли новый офис</span>
                <span class="news-newsfeed__card-description">С другой стороны консультация с широким активом в
                  значительной степени обуславливает создание направлений прогрессивного развития. Значимость этих
                  проблем настолько очевидна, что консультация с широким активом представляет собой интересный
                  эксперимент проверки существенных финансовых и административных условий. Идейные соображения высшего
                  порядка, а также сложившаяся структура организации способствует подготовки и реализации
                  соответствующий условий активизации. Таким образом дальнейшее развитие различных форм деятельности
                  позволяет оценить значение дальнейших направлений развития. Таким образом рамки и место обучения
                  кадров обеспечивает широкому кругу (специалистов) участие в формировании позиций, занимаемых
                  участниками в отношении поставленных задач. Идейные соображения высшего порядка, а также постоянный
                  количественный рост и сфера нашей активности в значительной степени обуславливает создание модели
                  развития.</span>
                <a href="#" class="news-newsfeed__card-button">подробней</a>
              </div>
              <!-- /.news-newsfeed__card -->
            </div>
          </div>
          <div class="news-previews">
            <span class="news-previews__title">Анонсы</span>
            <div class="news-previews__block wow fadeInUp" data-wow-delay="0.2s">
              <div class="news-previews__block-video">
                  <iframe style="width: 100%; height: 100%; border:0" src="https://www.youtube.com/embed/kelpJQRhrcA" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
              </div>
              <span class="news-previews__block-title">Рабочий процесс</span>
              <div class="news-previews__block-text">
                <a href="#" class="news-previews__block-past">предыдущее</a>
                <a href="#" class="news-previews__block-next">следующее</a>
              </div>
              <div class="news-previews__block-social">
                <a href="#" class="news-previews__block-social__icon">
                  <img src="img/news/vk.png" alt="vk">
                </a>
                <a href="#" class="news-previews__block-social__icon">
                  <img src="img/news/facebook.png" alt="facebook">
                </a>
                <a href="#" class="news-previews__block-social__icon">
                  <img src="img/news/instagram.png" alt="instagram">
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>

    <?php include 'consultation.php'; ?>

  </main>

  <?php include 'footer.php'; ?>
  
</body>

</html>