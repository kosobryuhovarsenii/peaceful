    <section class="service">
      <div class="container">
        <div class="service-block">
          <div class="service-head">
            <span class="service__title">Наши услуги</span>
            <div class="service-buttons">
              <a href="#" class="service-buttons__item">психология</a>
              <a href="#" class="service-buttons__item">психоаналитика</a>
              <a href="#" class="service-buttons__item">услуги медиатора</a>
            </div>
          </div>
          <div class="service-cards">
            <div class="service-cards__card wow fadeInUp" data-wow-delay="0.1s">
              <div class="service-cards__image">
                <img src="img/service/card-1.jpg" class="service-cards__image-item" alt="">
              </div>
              <div class="service-cards__text">
                <span class="service-cards__title">Тренинг <br> по осознанию души</span>
                <span class="service-cards__subtitle">Что такое душа? <br> Разберем все на практике</span>
                <span class="service-cards__price">Стоимость:</span>
                <span class="service-cards__value">5 400 руб.</span>
              </div>
            </div>
            <!-- /.service-cards__card -->
            <div class="service-cards__card wow fadeInUp" data-wow-delay="0.2s">
              <div class="service-cards__image">
                <img src="img/service/card-2.jpg" class="service-cards__image-item" alt="">
              </div>
              <div class="service-cards__text">
                <span class="service-cards__title">Тренинг <br> по осознанию души</span>
                <span class="service-cards__subtitle">Что такое душа? <br> Разберем все на практике</span>
                <span class="service-cards__price">Стоимость:</span>
                <span class="service-cards__value">5 400 руб.</span>
              </div>
            </div>
            <!-- /.service-cards__card -->
            <div class="service-cards__card wow fadeInUp" data-wow-delay="0.3s">
              <div class="service-cards__image">
                <img src="img/service/card-3.jpg" class="service-cards__image-item" alt="">
              </div>
              <div class="service-cards__text">
                <span class="service-cards__title">Тренинг <br> по осознанию души</span>
                <span class="service-cards__subtitle">Что такое душа? <br> Разберем все на практике</span>
                <span class="service-cards__price">Стоимость:</span>
                <span class="service-cards__value">5 400 руб.</span>
              </div>
            </div>
            <!-- /.service-cards__card -->
            <div class="service-cards__card wow fadeInUp" data-wow-delay="0.4s">
              <div class="service-cards__image">
                <img src="img/service/card-4.jpg" class="service-cards__image-item" alt="">
              </div>
              <div class="service-cards__text">
                <span class="service-cards__title">Тренинг <br> по осознанию души</span>
                <span class="service-cards__subtitle">Что такое душа? <br> Разберем все на практике</span>
                <span class="service-cards__price">Стоимость:</span>
                <span class="service-cards__value">5 400 руб.</span>
              </div>
            </div>
            <!-- /.service-cards__card -->
          </div>
        </div>
      </div>
    </section>