<!DOCTYPE html>
<html lang="ru">

<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <link rel="stylesheet" href="css/style.css">
  <title>Миролюбец</title>
</head>

<body>

  <?php include 'header.php'; ?>

  <?php include 'navbar.php'; ?>

  <main>

    <?php include 'service.php'; ?>

    <section class="poster">
        <div class="container">
          <div class="poster-block">
            <div class="poster-text wow bounceInLeft" data-wow-delay="0.2s">
              <h2 class="poster-text__subtitle">специальное предложение</h2>
              <h1 class="poster-text__title">
                <span class="poster-text__title-string1">Не будь</span>
                <span class="poster-text__title-string2">зависим</span>
                <span class="poster-text__title-string3">от сигарет</span>
              </h1>
              <a href="#" class="poster-text__button dialog-button">Подробней</a>
            </div>
          </div>
        </div>
    </section>

    <?php include 'consultation.php'; ?>

  </main>

  <?php include 'footer.php'; ?>
  
</body>

</html>